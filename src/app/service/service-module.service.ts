import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServiceModuleService {

  private getValue$: Observable<any>;s
  private getValueSource:  BehaviorSubject<any>;

  constructor(
    private router: Router) {
      this.getValueSource = new BehaviorSubject<any>(false);
      this.getValue$ = this.getValueSource.asObservable();
    }


  public setValue(): any{
    return this.getValueSource.value;
  }

  public serviceHandlerTrue(value: any): void{

      const previous = this.getValueSource.value;
      this.getValueSource .next(value);

      if(previous === this.getValueSource.value){
        return;
      }

      const i = this.router.config.findIndex(x => x.path === 'projects');
      this.router.config.splice(i, 1);
      this.router.config.push(
        {path: 'projects', loadChildren: () => import('../pojectHandlerModule/projects-handler.module').then(m => m.ProjectsHandlerModule)}
      );

  }

  
}
