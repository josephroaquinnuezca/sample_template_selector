import { ServiceModuleService } from './../service/service-module.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, ROUTES } from '@angular/router';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    {
      provide: ROUTES,
      useFactory: handlerModule,
      deps: [ServiceModuleService],
      multi: true
    }
  ]
})
export class ProjectModuleHandlerModule {}

export function handlerModule(service: ServiceModuleService) {

  let routes: Routes = [];

  if (service.setValue() == 't1') {
      routes = [
        {
          path: '', loadChildren: () => import('../Projects/LssOne/template-one.module').then(m => m.TemplateOneModule)
        }
      ];
  }else if(service.setValue() == 't2'){
    routes = [
        {
          path: '', loadChildren: () => import('../Projects/projectTwo/project-two-mdule.module').then(m => m.ProjectTwoMduleModule)
        }
    ];
  }
  return routes;

}s
